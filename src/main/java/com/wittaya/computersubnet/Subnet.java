/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wittaya.computersubnet;

/**
 *
 * @author AdMiN
 */
import java.util.Scanner;

 
public class Subnet {
    public static void calAddress(String Ip4) {
        String[] slash = Ip4.split("/");
        String Ip = slash[0];
        String CIDR = slash[1];
        String[] Bslash = Ip.split("\\.");
        int mask = Integer.parseInt(CIDR);
        int typeClass = Integer.parseInt(Bslash[0]);
        if (typeClass > 0 && typeClass <= 127){
            System.out.println("Class A");
        }else if (typeClass > 127 && typeClass <= 191){
            System.out.println("Class B");
        }else if (typeClass > 191 && typeClass <= 223){
            System.out.println("Class C");
        }else if (typeClass > 223 && typeClass <= 239){
            System.out.println("Class D");
        }else if (typeClass > 239 && typeClass <= 255){
            System.out.println("Class E");
        }
        if (mask > 0 && mask <= 8) {
            mask = calBinary(8-mask);
            int wc = 255-mask;
            int temp = Integer.parseInt(Bslash[0]);
            int bc = temp+wc;
            if (bc >= 255){
                bc = 255;
            }
            int na = bc-wc;
            System.out.println("Subnet = "+mask + ".0.0.0");
            System.out.println("Wildcard = "+wc+".255.255.255");
            System.out.println("Network address = "+na+".0.0.0");
            System.out.println("Broadcast address = "+bc+".255.255.255");
            System.out.println("HostMin = "+na+".0.0.1");
            System.out.println("HostMax = "+bc+".255.255.254");
        } else if (mask > 8 && mask <= 16) {
            mask = 16 - mask;
            calBinary(mask);
            mask = calBinary(mask);
            int wc = 255-mask;
            int temp = Integer.parseInt(Bslash[1]);
            int bc = temp+wc;
            if (bc >= 255){
                bc = 255;
            }
            int na = bc-wc;
            System.out.println("Subnet = "+"255." + mask + ".0.0");
            System.out.println("Wildcard = 0."+wc+".255.255");
            System.out.println("Network address = "+Bslash[0]+"."+na+".0.0");
            System.out.println("Broadcast address = "+Bslash[0]+"."+bc+".255.255");
            System.out.println("HostMin = "+Bslash[0]+"."+na+".0.1");
            System.out.println("HostMax = "+Bslash[0]+"."+bc+".255.254");
        } else if (mask > 16 && mask <= 24) {
            mask = 24 - mask;
            mask = calBinary(mask);
            int wc = 255-mask;
            int temp = Integer.parseInt(Bslash[2]);
            int bc = temp+wc;
            if (bc >= 255){
                bc = 255;
            }
            int na = bc-wc;
            System.out.println("Subnet = "+"255.255." + mask + ".0");
            System.out.println("Wildcard = 0.0."+wc+".255");
            System.out.println("Network address = "+Bslash[0]+"."+Bslash[1]+"."+na+".0");
            System.out.println("Broadcast address = "+Bslash[0]+"."+Bslash[1]+"."+bc+".255");
            System.out.println("HostMin = "+Bslash[0]+"."+Bslash[1]+"."+na+".1");
            System.out.println("HostMax = "+Bslash[0]+"."+Bslash[1]+"."+bc+".254");
        } else if (mask > 24 && mask <= 32) {
            mask = 32 - mask;
            mask = calBinary(mask);
            int wc = 255-mask;
            int temp = Integer.parseInt(Bslash[3]);
            int bc = temp+wc;
            if (bc >= 255){
                bc = 255;
            }
            int na = bc-wc;
            System.out.println("Subnet = "+"255.255.255." + mask);
            System.out.println("Wildcard = 0.0.0."+wc);
            if (mask == 255){
                System.out.println("Network address = "+Ip);
                System.out.println("Broadcast address = "+Ip);
                System.out.println("HostMin = "+Bslash[0]+"."+Bslash[1]+"."+Bslash[2]+"."+(na+1));
                System.out.println("HostMax = "+Bslash[0]+"."+Bslash[1]+"."+Bslash[2]+"."+Bslash[3]);
            }else{
                System.out.println("Network address = "+Bslash[0]+"."+Bslash[1]+"."+Bslash[2]+"."+na);
                System.out.println("Broadcast address = "+Bslash[0]+"."+Bslash[1]+"."+Bslash[2]+"."+bc);
                System.out.println("HostMin = "+Bslash[0]+"."+Bslash[1]+"."+Bslash[2]+"."+(na+1));
                System.out.println("HostMax = "+Bslash[0]+"."+Bslash[1]+"."+Bslash[2]+"."+(bc-1));
            }
        } else {
            System.out.println("Wrong ip!");
        }
    }

   public static int calBinary(int mask) {
        double calSub = 0;
        for (int i = 7; i >= mask; i--) {
            calSub = calSub + Math.pow(2, i);
        }
        int ans = (int) calSub;
        return ans;
    }

    public static void main(String[] args) {
        Scanner ip = new Scanner(System.in);
        System.out.print("Enter IP: ");
        String ipAddress = ip.next();
        calAddress(ipAddress);
    }

    
}


